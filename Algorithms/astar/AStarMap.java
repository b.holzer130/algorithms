import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;


/** 
 * This is map providing shortest path calculations based on AStar algorithm. 
 * The algorithm itself is inspired by https://de.wikipedia.org/wiki/A*-Algorithmus.
 * The (commented) main in just is a test based on the example given in the Wikipedia page.
 * The main driver for the AStar are coding contests where you have to find paths on map, e.g. to control a player.
 * Here's an example for such contests: https://www.codingame.com/multiplayer/bot-programming/vindinium.
 * As the input data is usually given in the form of Strings, I honor this in {@link AStarMap#AStarMap(List)}.
 * Look {@link AStarMap#main(String[])} for examples.<br/>
 * Note that this implementation is not threadsafe or provides exception handling that you might expect from a public code lib. */
class AStarMap {
	char[][] data;
	AStarGraph<Point> graph;
	Map<Character, List<Point>> points = new HashMap<Character, List<Point>>();
	AStar aStar;

	public AStarMap(List<String> encodedMap) {
		int sizeY = encodedMap.size();
		int sizeX = encodedMap.get(0).length();
		graph = new AStarGraph(true);
		data = new char[sizeX][sizeY];
		for (int y = 0; y < sizeY; y++) {
			String s = encodedMap.get(y);
			for (int x = 0; x < sizeX; x++) {
				char charAt = s.charAt(x);
				data[x][y] = charAt;
				if (charAt != ' ') {
					Point p = new Point(x, y);
					graph.addNode(p);
					if (!points.containsKey(charAt)) {
						points.put(charAt, new ArrayList<Point>());
					}
					points.get(charAt).add(p);
				}
			}
		}
		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < sizeX; x++) {
				Point from = new Point(x, y);
				Point hor = new Point(x + 1, y);
				Point vert = new Point(x, y + 1);
				if (graph.getNode(from) != null && graph.getNode(hor) != null) {
					graph.addEdge(from, hor, 1);
				}
				if (graph.getNode(from) != null && graph.getNode(vert) != null) {
					graph.addEdge(from, vert, 1);
				}
			}
		}
		aStar = new AStar<Point>(graph);

	}

	List<Point> path(Point from, Point to) {
		return aStar.path(from, to);
	}

	List<Point> pathTo(Point from, char target) {
		int m = Integer.MAX_VALUE;
		List<Point> minPath = null;
		for (Point potentialTarget : points.get(target)) {
			List<Point> candidate = path(from, potentialTarget);
			if (candidate.size() < m) {
				m = candidate.size();
				minPath = candidate;
			}
		}
		return minPath;
	}

	public static void main(String[] args) {
		List<String> map = new ArrayList<String>();
		map.add("* * *");
		map.add("* ***");
		map.add("*** * ");
		AStarMap m = new AStarMap(map);
		List<Point> path = m.path(new Point(0, 0), new Point(4, 0));
		visualizePathInMap(map, path);

		map = new ArrayList<String>();
		map.add("* * B");
		map.add("* ***");
		map.add("*A* A ");
		m = new AStarMap(map);
		visualizePathInMap(map, m.pathTo(new Point(0,1), 'A'));
		visualizePathInMap(map, m.pathTo(new Point(0,1), 'B'));
		visualizePathInMap(map, m.pathTo(new Point(4,1), 'A'));

	}

	static void visualizePathInMap(List<String> mmap, List<Point> path) {
		List<String> mapCopy = new ArrayList<String> (mmap);
		for (int i = 0; i < path.size(); i++) {
			Point p = path.get(i);
			char[] chars = mapCopy.get(p.y).toCharArray();
			chars[p.x] = (char) (48 + i);
			mapCopy.set(p.y, new String(chars));
		}
		System.out.println();
		for (String s : mapCopy) {
			System.out.println(s);
		}
	}
}


@SuppressWarnings({ "unchecked", "rawtypes" })
class AStar<T> {
	AStarGraph<T> graph;

	public AStar(AStarGraph<T> graph) {
		this.graph = graph;
	}

	@SuppressWarnings("unchecked")
	List<T> path(T from, T to) {
		graph.reset();
		PriorityQueue<AStarNode<T>> openList = new PriorityQueue<AStarNode<T>>();
		Set<AStarNode<T>> closedList = new HashSet<AStarNode<T>>();
		openList.add(graph.getNode(from));
		while (!openList.isEmpty()) {
			AStarNode<T> current = openList.poll();
			if (current.wrappedObject.equals(to)) {
				List<T> ret = new ArrayList<T>();
				do {
					ret.add(current.wrappedObject);
					current = current.predecessor;
				} while (current != null);
				Collections.reverse(ret);
				return ret;
			}
			closedList.add(current);
			expand(current, openList, closedList);
		}
		return null;
	}

	void expand(AStarNode<T> n, PriorityQueue<AStarNode<T>> openList, Set<AStarNode<T>> closedList) {
		for (AStarEdge e : graph.getEdges(n)) {
			if (closedList.contains(e.to)) {
				continue;
			}
			double tentative_g = n.g + e.weight;
			if (openList.contains(e.to) && tentative_g > e.to.f()) {
				continue;
			}
			e.to.predecessor = e.from;
			e.to.g = tentative_g;
			openList.remove(e.to);
			openList.add(e.to);
		}

	}
	/*
	 * public static void main(String[] args) { AStarGraph g = new AStarGraph(true);
	 * g.addNode("Saarbr�cken"); g.addNode("Kaiserslautern");
	 * g.addNode("Frankfurt"); g.addNode("Ludwigshafen"); g.addNode("W�rzburg");
	 * g.addNode("Heilbronn"); g.addNode("Karlsruhe"); g.addEdge("Saarbr�cken",
	 * "Karlsruhe", 145); g.addEdge("Saarbr�cken", "Kaiserslautern", 70);
	 * g.addEdge("Kaiserslautern", "Ludwigshafen", 53); g.addEdge("Kaiserslautern",
	 * "Frankfurt", 103); g.addEdge("Frankfurt", "W�rzburg", 116);
	 * g.addEdge("Ludwigshafen", "W�rzburg", 183); g.addEdge("Heilbronn",
	 * "W�rzburg", 102); g.addEdge("Heilbronn", "Karlsruhe", 84); AStar<String>
	 * astar = new AStar<String>(g); System.out.println(astar.path("Saarbr�cken",
	 * "W�rzburg")); }
	 */

}

@SuppressWarnings({ "unchecked", "rawtypes" })
class AStarGraph<T> {
	Map<T, AStarNode<T>> nodes = new HashMap<T, AStarNode<T>>();
	Map<T, List<AStarEdge<T>>> edgesFrom = new HashMap<T, List<AStarEdge<T>>>();
	boolean isBidirectional;

	public AStarGraph(boolean isBidirectional) {
		this.isBidirectional = isBidirectional;
	}

	public void addNode(T wrappedObject) {
		nodes.put(wrappedObject, new AStarNode(wrappedObject));
	}

	public void addEdge(T from, T to, double weight) {
		doAdd(from, to, weight);
		if (isBidirectional) {
			doAdd(to, from, weight);
		}
	}

	private void doAdd(T from, T to, double weight) {
		if (!edgesFrom.containsKey(from)) {
			edgesFrom.put(from, new ArrayList<AStarEdge<T>>());
		}
		edgesFrom.get(from).add(new AStarEdge(nodes.get(from), nodes.get(to), weight));
	}

	AStarNode getNode(T obj) {
		return nodes.get(obj);
	}
	
	void reset() {
		for(AStarNode n: nodes.values()) {
			n.predecessor=null;
			n.g=0;
		}
	}

	List<AStarEdge<T>> getEdges(AStarNode n) {
		return edgesFrom.get(n.wrappedObject);
	}

}


@SuppressWarnings("rawtypes")
class AStarEdge<T> {
	final AStarNode<T> from, to;
	double weight;

	public AStarEdge(AStarNode<T> from, AStarNode<T> to, double weight) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AStarEdge) {
			AStarEdge other = (AStarEdge) obj;
			return to.equals(other.to) && from.equals(other.from);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return from.hashCode() + 7 * to.hashCode();
	}
}

@SuppressWarnings("rawtypes")
class AStarNode<T> implements Comparable<AStarNode> {
	final T wrappedObject;
	double g = 0;
	double h = 0;
	AStarNode predecessor;

	public AStarNode(T wrappedObject) {
		this.wrappedObject = wrappedObject;
	}

	@Override
	public int compareTo(AStarNode o) {
		return Double.compare(f(), o.f());
	}

	double f() {
		return g + h;
	}

	T getWrapped() {
		return wrappedObject;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AStarNode) {
			return wrappedObject.equals(((AStarNode) obj).getWrapped());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return wrappedObject.hashCode();
	}

	@Override
	public String toString() {
		return "Node [" + wrappedObject + "]";
	}
}
